//use diesel::prelude::*;
use serde_derive::Serialize;
use serde_derive::Deserialize;
use crate::schema::posts;
//use diesel::Queryable;

//#[derive(Queryable)]
#[derive(PartialEq, Debug, Clone, Queryable,  Insertable,  AsChangeset, Serialize,  Deserialize, Associations, AsExpression)]
#[diesel(primary_key(id))]
#[diesel(table_name=posts)]
pub struct Post {
    pub id: i32,
    pub uuid: uuid::Uuid
}

#[derive(PartialEq, Debug, Clone, Queryable,  Insertable, AsChangeset,   Associations,   AsExpression )]
#[table_name="posts"]
pub struct NewPost { 
    pub uuid: uuid::Uuid,
}
