//use diesel::{delete, pg::expression::dsl::all};


#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_migrations;
//use diesel_migrations::EmbedMigrations;
diesel_migrations::embed_migrations!("migrations");

pub mod models;
pub mod schema;

use models::{NewPost, Post};

fn main() {
    println!("Hello, world!");
    let _conn = &mut establish_connection();

    let _new_post = insert_id(_conn, uuid::Uuid::new_v4()  );

}

use diesel::prelude::*;
use dotenv::dotenv;
use std::env;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}


pub fn insert_id(conn: &mut PgConnection, uuid: uuid::Uuid) -> Post {
    use crate::schema::posts;

    let new_post = NewPost { uuid } ; 

    diesel::insert_into(posts::table)
        .values(&new_post)
        .get_result(conn)
        .expect("Error saving new post")
}
